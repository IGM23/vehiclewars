#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "VW_ShootComponent.generated.h"

class AVehicleWarsBullet;

UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VEHICLEWARS_API UVW_ShootComponent : public UActorComponent
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TSubclassOf<AVehicleWarsBullet> BulletToSpawnClass = nullptr;

public:	
	UVW_ShootComponent();
	
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION()
	void Shoot();
	
	UFUNCTION(Server, Reliable)
	void Shoot_Server();

	UFUNCTION()
	void BulletHitHandle(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet);

	UFUNCTION(Server, Reliable)
	void BulletHitHandle_Server(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet);

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void Shoot_Internal_FromServer();

	UFUNCTION()
	void BulletHitHandle_Internal_FromServer(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet);
};
