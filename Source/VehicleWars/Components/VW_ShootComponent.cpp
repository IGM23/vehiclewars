#include "VW_ShootComponent.h"

#include "Kismet/GameplayStatics.h"
#include "VehicleWars/Characters/VehicleWarsCharacter.h"
#include "VehicleWars/Actors/VehicleWarsBullet.h"
#include "VehicleWars/GameStates/VehicleWarsGameState.h"

UVW_ShootComponent::UVW_ShootComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UVW_ShootComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UVW_ShootComponent::Shoot()
{
	if(GetOwnerRole() == ROLE_Authority)
		Shoot_Internal_FromServer();
	else
		Shoot_Server();
}

void UVW_ShootComponent::Shoot_Server_Implementation()
{
	Shoot_Internal_FromServer();
}

void UVW_ShootComponent::BulletHitHandle(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet)
{
	if(GetOwnerRole() == ROLE_Authority)
		BulletHitHandle_Internal_FromServer(target, bullet);
	else
		BulletHitHandle_Server(target, bullet);
}

void UVW_ShootComponent::BulletHitHandle_Server_Implementation(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet)
{
	BulletHitHandle_Internal_FromServer(target, bullet);
}

void UVW_ShootComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UVW_ShootComponent::Shoot_Internal_FromServer()
{
	AVehicleWarsCharacter* ownerCharacter = Cast<AVehicleWarsCharacter>(GetOwner());

	if(ownerCharacter)
	{
		AVehicleWarsBullet* spawnedBullet = GetWorld()->SpawnActorDeferred<AVehicleWarsBullet>(BulletToSpawnClass.Get(),
		ownerCharacter->GetActorTransform(), ownerCharacter, ownerCharacter);

		spawnedBullet->SetOwnerVehicle(ownerCharacter);

		UGameplayStatics::FinishSpawningActor(spawnedBullet, ownerCharacter->GetActorTransform());
	}
}

void UVW_ShootComponent::BulletHitHandle_Internal_FromServer(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet)
{
	if(target)
	{
		AVehicleWarsCharacter* ownerCharacter = Cast<AVehicleWarsCharacter>(GetOwner());
		AVehicleWarsGameState* gameState = Cast<AVehicleWarsGameState>(GetWorld()->GetGameState());
		gameState->OnVehicleDestroyedHandle_FromServer(target, ownerCharacter);
	}
	bullet->DestroyBullet();
}
