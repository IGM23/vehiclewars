#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "VehicleWarsCharacter.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UVW_ShootComponent;
class AVehicleWarsBullet;
class AVehicleWarsSpawner;
class UPathFollowingComponent;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnVehicleDestroyed);

UCLASS(Blueprintable, BlueprintType)
class AVehicleWarsCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	FOnVehicleDestroyed OnVehicleDestroyedDelegate;

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Shooting, meta = (AllowPrivateAccess = "true"))
	UVW_ShootComponent* ShootComponent;

public:
	AVehicleWarsCharacter();

	UFUNCTION()
	void Shoot() const;

	UFUNCTION()
	void OnBulletHitHandle(AVehicleWarsCharacter* target, AVehicleWarsBullet* bullet) const;
	
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};

