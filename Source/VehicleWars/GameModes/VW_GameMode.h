#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "VW_GameMode.generated.h"

class AVehicleWarsSpawner;

UCLASS()
class VEHICLEWARS_API AVW_GameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
	AVW_GameMode();

protected:
	virtual void StartPlay() override;
	
};
