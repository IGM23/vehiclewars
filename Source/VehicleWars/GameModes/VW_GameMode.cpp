#include "VW_GameMode.h"

#include "VehicleWars/Controllers/VehicleWarsPlayerController.h"

AVW_GameMode::AVW_GameMode()
{
	PlayerControllerClass = AVehicleWarsPlayerController::StaticClass();
	
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownCharacter"));
	if (PlayerPawnBPClass.Class != nullptr)
		DefaultPawnClass = PlayerPawnBPClass.Class;
	
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/TopDown/Blueprints/BP_TopDownPlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
		PlayerControllerClass = PlayerControllerBPClass.Class;
}

void AVW_GameMode::StartPlay()
{
	Super::StartPlay();
}
