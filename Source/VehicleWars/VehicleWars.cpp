// Copyright Epic Games, Inc. All Rights Reserved.

#include "VehicleWars.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VehicleWars, "VehicleWars" );

DEFINE_LOG_CATEGORY(LogVehicleWars)
 