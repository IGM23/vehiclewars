#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameState.h"
#include "VehicleWarsGameState.generated.h"

class AVehicleWarsPlayerController;
class AVehicleWarsSpawner;
class AVehicleWarsCharacter;
class AVehicleWarsBullet;

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnAllSpawnersAdded);

UCLASS(Blueprintable, BlueprintType)
class VEHICLEWARS_API AVehicleWarsGameState : public AGameState
{
	GENERATED_BODY()

public:
	FOnAllSpawnersAdded OnOnAllSpawnersAddedDelegate;

private:
	UPROPERTY()
	TMap<APlayerController*, UClass*> PlayerClassesMap;

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	TArray<TSubclassOf<AVehicleWarsCharacter>> VehicleToSpawnClasses;
	
	UPROPERTY()
	TArray<AVehicleWarsSpawner*> Spawners;

	UPROPERTY(Replicated)
	bool AllSpawnersReady = false;

public:
	UFUNCTION(BlueprintCallable)
	void AddSpawner(AVehicleWarsSpawner* spawner);

	UFUNCTION(BlueprintCallable)
	void BroadCastSpawnersReady();
	
	UFUNCTION()
	void SpawnPlayer_FromServer(AVehicleWarsPlayerController* playerController);

	UFUNCTION()
	void OnVehicleDestroyedHandle_FromServer(AVehicleWarsCharacter* vehicleDestroyed, AVehicleWarsCharacter* vehicleInstigator);

	UFUNCTION()
	bool GetAllSpawnersReady() const { return AllSpawnersReady; }

	UFUNCTION()
	void UpdateHUD_FromServer();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	UFUNCTION()
	void UpdateScore(AVehicleWarsCharacter* vehicleWhoWinPoint);

	UFUNCTION()
	int32 GetValidSpawnerIndex();

	virtual void BeginPlay() override;
};
