#include "VehicleWarsGameState.h"

#include "GameFramework/PlayerState.h"
#include "Net/UnrealNetwork.h"
#include "VehicleWars/Characters/VehicleWarsCharacter.h"
#include "VehicleWars/Controllers/VehicleWarsPlayerController.h"
#include "VehicleWars/Actors/VehicleWarsSpawner.h"

void AVehicleWarsGameState::AddSpawner(AVehicleWarsSpawner* spawner)
{
	Spawners.AddUnique(spawner);
}

void AVehicleWarsGameState::BroadCastSpawnersReady()
{
	AllSpawnersReady = true;
	OnOnAllSpawnersAddedDelegate.Broadcast();
}

void AVehicleWarsGameState::SpawnPlayer_FromServer(AVehicleWarsPlayerController* playerController)
{
	UClass* classToSpawn = nullptr;
	if(UClass** classFound = PlayerClassesMap.Find(playerController))
		classToSpawn = *classFound;

	if(!classToSpawn && VehicleToSpawnClasses.Num() > 0)
	{
		const int32 randomClassIndex = FMath::RandRange(0, VehicleToSpawnClasses.Num() - 1);
		classToSpawn = VehicleToSpawnClasses[randomClassIndex].Get();
		PlayerClassesMap.Add(playerController, classToSpawn);

		VehicleToSpawnClasses.RemoveAt(randomClassIndex);
	}

	if(classToSpawn)
	{
		const int32 randomSpawnerIndex = GetValidSpawnerIndex();
		AVehicleWarsCharacter* spawnedVehicle = Spawners[randomSpawnerIndex]->SpawnVehicle(classToSpawn);

		if(playerController)
			playerController->Possess(spawnedVehicle);
	}
}

void AVehicleWarsGameState::OnVehicleDestroyedHandle_FromServer(AVehicleWarsCharacter* vehicleDestroyed,
	AVehicleWarsCharacter* vehicleInstigator)
{
	UpdateScore(vehicleInstigator);

	if(vehicleDestroyed)
	{
		APlayerState* playerState = vehicleDestroyed->GetPlayerState();
		if(playerState)
		{
			AVehicleWarsPlayerController* playerController = Cast<AVehicleWarsPlayerController>(playerState->GetPlayerController());
			vehicleDestroyed->Destroy();

			SpawnPlayer_FromServer(playerController);
		}
	}
}

void AVehicleWarsGameState::UpdateHUD_FromServer()
{
	TArray<FString> namesArray;
	TArray<float> scoresArray;
	
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		namesArray.Add(playerStatePtr->GetHumanReadableName());
		scoresArray.Add(playerStatePtr->GetScore());
	}
	
	for (TObjectPtr<APlayerState> playerStatePtr : PlayerArray)
	{
		AVehicleWarsPlayerController* playerController = Cast<AVehicleWarsPlayerController>(playerStatePtr->GetPlayerController());
		if(playerController)
			playerController->UpdatePlayersHUD_Multicast(namesArray, scoresArray);
	}
}

void AVehicleWarsGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVehicleWarsGameState, AllSpawnersReady);
}

void AVehicleWarsGameState::UpdateScore(AVehicleWarsCharacter* vehicleWhoWinPoint)
{
	const float currentScore = vehicleWhoWinPoint->GetPlayerState()->GetScore();
	const float newScore = currentScore + 1.0f;
	vehicleWhoWinPoint->GetPlayerState()->SetScore(newScore);
}

int32 AVehicleWarsGameState::GetValidSpawnerIndex()
{
	int32 randomSpawnerIndex = FMath::RandRange(0, Spawners.Num() - 1);
	
	for(int32 tries = 0; tries < Spawners.Num(); ++tries)
	{
		if(!Spawners[randomSpawnerIndex]->IsPlayerNearby())
			return randomSpawnerIndex;

		++randomSpawnerIndex;
		if(randomSpawnerIndex == Spawners.Num())
			randomSpawnerIndex = 0;
	}

	// If all spawners are occupied, spawn anyways in the first one.
	return 0;
}

void AVehicleWarsGameState::BeginPlay()
{
	Super::BeginPlay();
}
