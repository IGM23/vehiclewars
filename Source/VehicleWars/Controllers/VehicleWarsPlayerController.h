#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "VehicleWarsPlayerController.generated.h"

class UNiagaraSystem;
class UInputMappingContext;
class UInputAction;

UCLASS()
class AVehicleWarsPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	/** MappingContext */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputMappingContext* DefaultMappingContext;
	
	/** Click Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* SetDestinationClickAction;

	/** Shoot Input Action */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
	UInputAction* ShootAction;

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

private:
	FVector CachedDestination;

public:
	AVehicleWarsPlayerController();

	virtual void BeginPlay();

	virtual void SetupInputComponent() override;

	UFUNCTION()
	void OnShootStarted();
	
	/** Input handlers for SetDestination action. */
	UFUNCTION()
	void OnSetDestinationTriggered();
	UFUNCTION()
	void OnSetDestinationReleased();

	/** Unreal Engine only updates navigation on 'OnPossess' on the server,
	 * so we have to update it on the clients as well.
	 */
	virtual void OnRep_Pawn() override;
	
	UFUNCTION(Server, Reliable)
	void UpdateClientHUDOnStart_Server();

	UFUNCTION(NetMulticast, Reliable)
	void UpdatePlayersHUD_Multicast(const TArray<FString>& names, const TArray<float>& scores);

protected:
	virtual void OnPossess(APawn* aPawn) override;
	
	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePlayersHUD_Blueprint(const TArray<FString>& names, const TArray<float>& scores);
	
	UFUNCTION()
	void OnGameStateAllSpawnersReady_FromServer();

private:
	UFUNCTION()
	void UpdateHUD_Internal();
};
