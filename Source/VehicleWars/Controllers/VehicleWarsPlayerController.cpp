#include "VehicleWarsPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "NiagaraFunctionLibrary.h"
#include "VehicleWars/Characters/VehicleWarsCharacter.h"
#include "Engine/World.h"
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "VehicleWars/GameStates/VehicleWarsGameState.h"
#include "Navigation/PathFollowingComponent.h"

AVehicleWarsPlayerController::AVehicleWarsPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Default;
	CachedDestination = FVector::ZeroVector;
	bReplicates = true;
}

void AVehicleWarsPlayerController::BeginPlay()
{
	Super::BeginPlay();

	//Add Input Mapping Context
	if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(GetLocalPlayer()))
	{
		Subsystem->AddMappingContext(DefaultMappingContext, 0);
	}
	
	if(GetLocalRole() == ROLE_Authority)
	{
		if(GetPawn() == nullptr)
		{
			AVehicleWarsGameState* gameState = Cast<AVehicleWarsGameState>(GetWorld()->GetGameState());
			
			// If the spawners are ready, spawn a player. If not, do it as soon as they are ready.
			if(gameState)
			{
				if(gameState->GetAllSpawnersReady())
					gameState->SpawnPlayer_FromServer(this);
				else
					gameState->OnOnAllSpawnersAddedDelegate.AddDynamic(this, &AVehicleWarsPlayerController::OnGameStateAllSpawnersReady_FromServer);
			}
		}
	}
	else
	{
		UpdateClientHUDOnStart_Server();
	}
}

void AVehicleWarsPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	// Set up action bindings
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
	{
		// Setup mouse input events
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Triggered, this, &AVehicleWarsPlayerController::OnSetDestinationTriggered);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Completed, this, &AVehicleWarsPlayerController::OnSetDestinationReleased);
		EnhancedInputComponent->BindAction(SetDestinationClickAction, ETriggerEvent::Canceled, this, &AVehicleWarsPlayerController::OnSetDestinationReleased);

		EnhancedInputComponent->BindAction(ShootAction, ETriggerEvent::Started, this, &AVehicleWarsPlayerController::OnShootStarted);
	}
}

void AVehicleWarsPlayerController::OnShootStarted()
{
	AVehicleWarsCharacter* controlledCharacter = Cast<AVehicleWarsCharacter>(GetPawn());
	if(controlledCharacter)
		controlledCharacter->Shoot();
}

// Triggered every frame when the input is held down
void AVehicleWarsPlayerController::OnSetDestinationTriggered()
{
	// We look for the location in the world where the player has pressed the input
	FHitResult Hit;
	bool bHitSuccessful = GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, true, Hit);

	// If we hit a surface, cache the location
	if (bHitSuccessful)
		CachedDestination = Hit.Location;
}

void AVehicleWarsPlayerController::OnSetDestinationReleased()
{
	UNiagaraFunctionLibrary::SpawnSystemAtLocation(this, FXCursor, CachedDestination, FRotator::ZeroRotator,
		FVector(1.f, 1.f, 1.f), true, true, ENCPoolMethod::None, true);

	UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, CachedDestination);
}

void AVehicleWarsPlayerController::OnRep_Pawn()
{
	Super::OnRep_Pawn();
	
	if(GetLocalRole() != ROLE_Authority)
	{
		UPathFollowingComponent* PathFollowingComp = FindComponentByClass<UPathFollowingComponent>();
		if (PathFollowingComp)
			PathFollowingComp->UpdateCachedComponents();
	}
}

void AVehicleWarsPlayerController::UpdateClientHUDOnStart_Server_Implementation()
{
	UpdateHUD_Internal();
}

void AVehicleWarsPlayerController::UpdatePlayersHUD_Multicast_Implementation(const TArray<FString>& names, const TArray<float>& scores)
{
	if(IsLocalPlayerController())
		UpdatePlayersHUD_Blueprint(names, scores);
}

void AVehicleWarsPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);
	if(GetLocalRole() == ROLE_Authority)
		UpdateHUD_Internal();
}

void AVehicleWarsPlayerController::OnGameStateAllSpawnersReady_FromServer()
{
	AVehicleWarsGameState* gameState = Cast<AVehicleWarsGameState>(GetWorld()->GetGameState());
	if(gameState)
	{
		gameState->SpawnPlayer_FromServer(this);
		gameState->OnOnAllSpawnersAddedDelegate.RemoveDynamic(this, &AVehicleWarsPlayerController::OnGameStateAllSpawnersReady_FromServer);
	}
}

void AVehicleWarsPlayerController::UpdateHUD_Internal()
{
	AVehicleWarsGameState* gameState = Cast<AVehicleWarsGameState>(GetWorld()->GetGameState());
	if(gameState)
		gameState->UpdateHUD_FromServer();
}
