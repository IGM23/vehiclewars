#include "VehicleWarsBullet.h"
#include "Components/SphereComponent.h"
#include "Net/UnrealNetwork.h"
#include "VehicleWars/Characters/VehicleWarsCharacter.h"

AVehicleWarsBullet::AVehicleWarsBullet()
{
	PrimaryActorTick.bCanEverTick = true;

	bReplicates = true;

	SphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("Sphere Component"));
	SphereComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereComp->SetupAttachment(RootComponent);
	SetRootComponent(SphereComp);
	
	StaticMeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh"));
	StaticMeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMeshComp->SetupAttachment(RootComponent); 
}

void AVehicleWarsBullet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AVehicleWarsBullet, IsAlive);
}

void AVehicleWarsBullet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FVector newLocation = GetActorForwardVector() * Speed * DeltaTime + GetActorLocation();
	SetActorLocation(newLocation);

	if(IsAlive)
	{
		Lifetime -= DeltaTime;
		if(Lifetime <= 0.f)
			DestroyBullet();
	}
}

void AVehicleWarsBullet::OnBulletCollision(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	if(OwnerVehicle && OwnerVehicle != OtherActor && this != OtherActor)
	{
		AVehicleWarsCharacter* hitVehicle = Cast<AVehicleWarsCharacter>(OtherActor);
		IsAlive = false;
		if(hitVehicle)
			OwnerVehicle->OnBulletHitHandle(hitVehicle, this);
		else
			OwnerVehicle->OnBulletHitHandle(nullptr, this);
	}
}

void AVehicleWarsBullet::DestroyBullet()
{
	SphereComp->OnComponentBeginOverlap.Clear();
	Destroy();
}

void AVehicleWarsBullet::BeginPlay()
{
	Super::BeginPlay();

	SphereComp->OnComponentBeginOverlap.AddDynamic(this, &AVehicleWarsBullet::OnBulletCollision);
}
