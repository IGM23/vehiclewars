#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VehicleWarsSpawner.generated.h"

class AVehicleWarsCharacter;

UCLASS(Blueprintable, BlueprintType)
class VEHICLEWARS_API AVehicleWarsSpawner : public AActor
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, meta = (AllowPrivateAccess = "true"))
	float DistanceToCheckPlayer = 250.f;
	
public:	
	AVehicleWarsSpawner();

	UFUNCTION()
	AVehicleWarsCharacter* SpawnVehicle(UClass* classToSpawn) const;

	UFUNCTION()
	bool IsPlayerNearby();
};
