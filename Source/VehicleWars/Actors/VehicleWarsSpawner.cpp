#include "VehicleWarsSpawner.h"

#include "Kismet/GameplayStatics.h"
#include "VehicleWars/Characters/VehicleWarsCharacter.h"

AVehicleWarsSpawner::AVehicleWarsSpawner()
{
	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;
}

AVehicleWarsCharacter* AVehicleWarsSpawner::SpawnVehicle(UClass* classToSpawn) const
{
	FActorSpawnParameters spawnParameters;
	AVehicleWarsCharacter* spawnedVehicle = GetWorld()->SpawnActor<AVehicleWarsCharacter>(classToSpawn, GetActorTransform(), spawnParameters);

	return spawnedVehicle;
}

bool AVehicleWarsSpawner::IsPlayerNearby()
{
	// 1- Sphere collision to search possible targets
	TArray<AActor*> possibleTargets;
	
	TArray<TEnumAsByte<EObjectTypeQuery>> traceObjectTypes;
	traceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));
	
	TArray<AActor*> ignoreActors;
	ignoreActors.Init(this, 1);

	UClass* seekClass = AVehicleWarsCharacter::StaticClass();
	
	UKismetSystemLibrary::SphereOverlapActors(GetWorld(), GetActorLocation(), DistanceToCheckPlayer,
		traceObjectTypes, seekClass, ignoreActors, possibleTargets);
	
	// 2- From Collided Actors, check that are players
	for(AActor* currentActor : possibleTargets)
	{
		AVehicleWarsCharacter* player = Cast<AVehicleWarsCharacter>(currentActor);
		if(player)
			return true;
	}

	return false;
}
