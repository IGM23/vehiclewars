#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "VehicleWarsBullet.generated.h"

class USphereComponent;
class UStaticMeshComponent;
class AVehicleWarsCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnVehicleHit, AVehicleWarsCharacter*, VehicleHit);

UCLASS(Blueprintable, BlueprintType)
class VEHICLEWARS_API AVehicleWarsBullet : public AActor
{
	GENERATED_BODY()

public:
	FOnVehicleHit OnVehicleHitDelegate;

private:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	UStaticMeshComponent* StaticMeshComp = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess = "true"))
	USphereComponent* SphereComp = nullptr;
	
	/** Centimeters per second. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	float Speed = 1000.f;

	/** Seconds that the bullet is alive before auto-destroying. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = true))
	float Lifetime = 5.f;

	UPROPERTY(Replicated)
	bool IsAlive = true;

	UPROPERTY()
	AVehicleWarsCharacter* OwnerVehicle = nullptr;
	
public:	
	AVehicleWarsBullet();

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnBulletCollision(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit);

	UFUNCTION()
	void DestroyBullet();

	UFUNCTION(BlueprintCallable)
	AVehicleWarsCharacter* GetOwnerVehicle(){ return OwnerVehicle; }
	UFUNCTION()
	void SetOwnerVehicle(AVehicleWarsCharacter* newOwner){ OwnerVehicle = newOwner; }

protected:
	virtual void BeginPlay() override;
};
